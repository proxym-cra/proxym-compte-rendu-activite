Feature: gestion des absences

    Example: ajout d'une absence pour une date
    Example: on associe une raison a une absence
    Example: ajout d'une absence partielle
    Example: ajout d'une absence pour une plage de dates
    Example: pas d'absence pendant les jours fériés
    Example: pas d'absence pendant les weekends
    Example: une seule absence par demie journee
    Example: pas plus de deux absences par jour
    Example: suppression d'une absence
    @ToDo
    Example: suppression des absences par plage de dates
    @ToDo
    Example: modification possible apres le 5 du mois suivant
    @ToDo
    Example: plage de date commencant l'apres-midi
    @ToDo
    Example: plage de date se terminant le matin